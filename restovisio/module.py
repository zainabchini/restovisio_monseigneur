# -*- coding: utf-8 -*-

# Copyright(C) 2018 Sasha Bouloudnine

from monseigneur.core.tools.backend import Module
from .browser import RestovisioBrowser

__all__ = ['RestovisioModule']


class RestovisioModule(Module):
	NAME = 'Restovisio'
	MAINTAINER = u'Zainab'
	EMAIL = '{first}.{last}@lobstr.io'
	BROWSER = RestovisioBrowser

	def go_restaurants(self, location, page):
		return self.browser.go_restaurants(location, page)

	def get_restaurants(self):
		return self.browser.get_restaurants()

	def iter_main_page(self):
		return self.browser.iter_main_page()

	def get_item_page(self, obj):
		return self.browser.get_item_page(obj)

	def go_menu_page(self, url):
		return self.browser.go_menu_page(url)