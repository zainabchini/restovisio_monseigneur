from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.mysql import MEDIUMTEXT, VARCHAR, BOOLEAN, DATETIME, LONGTEXT, INTEGER
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()



class Task(Base):
    __tablename__ = 'task'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}


    id = Column(INTEGER, primary_key=True, autoincrement=True)

    url = Column(MEDIUMTEXT)
    total_results = Column(MEDIUMTEXT)
    last_result_scraped = Column(MEDIUMTEXT)
    total_pages = Column(MEDIUMTEXT)
    last_page_scraped = Column(MEDIUMTEXT)
    start_time = Column(DATETIME)
    end_time = Column(DATETIME)
    elapsed = Column(VARCHAR(400))
    restaurant = relationship("Restaurant")


class Restaurant(Base):

    __tablename__ = 'restaurant'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}

    id = Column(INTEGER, primary_key=True, autoincrement=True)
    task_id = Column(INTEGER, ForeignKey('task.id'))
    url = Column(VARCHAR(350))


    type = Column(MEDIUMTEXT)
    nom = Column(LONGTEXT)
    adresse = Column(MEDIUMTEXT)
    cp = Column(MEDIUMTEXT)
    ville = Column(MEDIUMTEXT)
    departement = Column(MEDIUMTEXT)
    region = Column(MEDIUMTEXT)
    pays = Column(MEDIUMTEXT)
    telephone = Column(VARCHAR(1000))
    cadre_and_ambiance = Column(MEDIUMTEXT)
    cuisine = Column(MEDIUMTEXT)
    budget = Column(MEDIUMTEXT)
    vues = Column(MEDIUMTEXT)
    site_web = Column(MEDIUMTEXT)
    facebook = Column(MEDIUMTEXT)
    image = Column(LONGTEXT)
    video = Column(LONGTEXT)
    mots_cles = Column(MEDIUMTEXT)
    description = Column(LONGTEXT)
    parking = Column(MEDIUMTEXT)
    lat = Column(MEDIUMTEXT)
    lng = Column(MEDIUMTEXT)
    horaires_lundi = Column(MEDIUMTEXT)
    horaires_mardi = Column(MEDIUMTEXT)
    horaires_mercredi = Column(MEDIUMTEXT)
    horaires_jeudi = Column(MEDIUMTEXT)
    horaires_vendredi = Column(MEDIUMTEXT)
    horaires_samedi = Column(MEDIUMTEXT)
    horaires_dimanche = Column(MEDIUMTEXT)
    paiements = Column(LONGTEXT)
    services = Column(LONGTEXT)
    menu_url = Column(LONGTEXT)
    menu = relationship("Menu")

class Menu(Base):
    __tablename__ = 'menu'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}

    id = Column(INTEGER, primary_key=True, autoincrement=True)
    restaurant_id = Column(INTEGER, ForeignKey('restaurant.id'))

    name = Column(LONGTEXT)
    description = Column(LONGTEXT)
    price = Column(VARCHAR(1000))
    category = Column(MEDIUMTEXT)
    sub_category = Column(MEDIUMTEXT)
    last_update = Column(VARCHAR(400))
        

def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
