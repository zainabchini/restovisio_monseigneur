from mbackend.core.fetcher import Fetcher
from mbackend.core.application import Application
from monseigneur.modules.public.restovisio.alchemy.dao_manager import DaoManager
from monseigneur.modules.public.restovisio.alchemy.tables import Restaurant, Task, Menu
import time
import datetime


class RestovisioBackend(Application):
    

    APPNAME = "Application Restovisio"
    VERSION = '1.0'
    COPYRIGHT = 'Copyright(C) 2012-YEAR LOBSTR'
    DESCRIPTION = "Scraping Backend for Restovisio"
    SHORT_DESCRIPTION = "Step-by-step Example of Restovisio Scraping"

    def __init__(self):
        super(RestovisioBackend, self).__init__(self.APPNAME)
        self.setup_logging()
        self.fetcher = Fetcher(is_public=True)
        self.module = self.fetcher.build_backend("restovisio", params={})

        self.dao = DaoManager("restovisio")
        self.session, self.scoped_session = self.dao.get_shared_session()


    def main(self):
        count = 0
        for location in range(1, 101):
            main_url = "http://www.restovisio.com/search?sq=&location=%i&geoloc=0" %location 
            main_pages = self.module.go_restaurants(location=location, page=1)
            iter_main_page = self.module.iter_main_page()
            for page in iter_main_page:
                if not self.session.query(Task).filter(Task.url == main_url).count():
                    page.__dict__['url'] = main_url
                    page.__dict__['start_time'] = datetime.datetime.now()
                    self.session.add(page)
                    self.session.commit()
                    print("total entered")
                else:
                    continue

            pages = self.session.query(Task.total_pages).filter(Task.url == main_url)[0]
            for x in pages:
                if x:
                    page = int(x) + 1
                else:
                     page = 2
                     self.session.query(Task).filter(Task.url == main_url).update(
                        {'total_pages' : '1'})
                     self.session.commit()
                

            for page in range(1, page):
                print(page)
                restaurants = self.module.go_restaurants(location=location, page=page) 
                get_restaurant = self.module.get_restaurants()

                for restaurant in get_restaurant:
                    tasks = self.session.query(Task.id).filter(Task.url == main_url)[0]
                    get_item = self.module.get_item_page(restaurant)

                    

                    menu_url = 'http://www.restovisio.com/' + get_item.url +'#menu'
                    count = count + 1
                    for x in tasks:
                        task_id = x
                        get_item.__dict__['task_id'] = task_id
                        get_item.__dict__['menu_url'] = menu_url

                    print(get_item.__dict__)
                    
                    restaurant_list = self.session.query(Restaurant).filter(Restaurant.url == restaurant.url)
    
                    if not restaurant_list.count():
                        self.session.add(get_item)
                        self.session.commit()
                        print("item entered")
                        
                    else:
                        for y in restaurant_list:
                            print(y.url)
                        print("item exists")

                    menus = self.module.go_menu_page(get_item.url)

                    restaurant_menu = self.session.query(Restaurant.id).filter(Restaurant.menu_url == menu_url)[0]

                    for menu in menus:
                        for url in restaurant_menu:
                            menu.__dict__['restaurant_id'] = url

                        print(menu.__dict__)
                        menu_list = self.session.query(Menu).filter(Menu.name == menu.name, Menu.restaurant_id == menu.restaurant_id)
                        if not menu_list.count():
                            self.session.add(menu)
                            self.session.commit()
                            print("menu entered")
                            
                        else:
                            print("menu item exists")
            start_date = self.session.query(Task.start_time).filter(Task.url == main_url)[0]
            for date in start_date:
                end_date = datetime.datetime.now()
                total_time = (end_date - date).total_seconds()/60
            self.session.query(Task).filter(Task.url == main_url).update(
                {'total_results' : str(count),
                'end_time' : end_date,
                'last_result_scraped' : str(self.session.query(Restaurant.id).order_by(Restaurant.id.desc()).first()[0]),
                'last_page_scraped' : str(page),
                'elapsed' : str(total_time)
                })
            self.session.commit()
            count = 0

                










if __name__ == '__main__':
    my = RestovisioBackend()
    my.main()
