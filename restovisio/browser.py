# -*- coding: utf-8 -*-

# Copyright(C) 2018 Sasha Bouloudnine

from monseigneur.core.browser import PagesBrowser, URL
from .pages import ApiPage, ItemPage, MenuPage

__all__ = ['RestovisioBrowser']


class RestovisioBrowser(PagesBrowser):

    BASEURL = 'http://www.restovisio.com/'

    # step 1 : Iter departments


    
    menu_page = URL("(?P<url>.+)#menu", MenuPage)
    list_page = URL("/search/(?P<page>\d+).htm\?sq=&location=(?P<location>\d+)&geoloc=0", ApiPage)
    item_page = URL("(?P<url>.+)", ItemPage)
    
    def __init__(self, *args, **kwargs):
        super(RestovisioBrowser, self).__init__(*args, **kwargs)


    def go_restaurants(self, location, page):
        self.list_page.go(page=page, location=location)
        assert self.list_page.is_here()


    def get_restaurants(self):
        assert self.list_page.is_here()
        return self.page.iter_restaurants()

    def iter_main_page(self):
        assert self.list_page.is_here()
        return self.page.iter_main_page()

    def get_item_page(self, obj):
        self.item_page.go(url= obj.url)
        assert self.item_page.is_here()
        return self.page.get_item(obj)

    def go_menu_page(self, url):
        self.menu_page.go(url=url)
        assert self.menu_page.is_here()
        return self.page.iter_menu_page()

