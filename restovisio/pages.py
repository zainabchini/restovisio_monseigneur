from monseigneur.core.browser.pages import HTMLPage
import json
from monseigneur.core.browser.elements import ItemElement, ListElement, method, DictElement
from monseigneur.modules.public.restovisio.alchemy.tables import Restaurant, Task, Menu
from monseigneur.core.browser.filters.standard import CleanText, RawText, Type, CleanDate
import re

base_url = 'http://www.restovisio.com'
class ApiPage(HTMLPage):


    ENCODING = 'UTF8'
   
    def build_doc(self, text):
        return HTMLPage.build_doc(self, text)
        

    @method
    class iter_restaurants(ListElement):
        item_xpath = "//div[@class='item_infos']/div[@class='h3_st']"

        class get_restaurant(ItemElement):
            klass = Restaurant

            obj_nom = CleanText("./a/span")
            def obj_url(self):
                
                url = CleanText("./a/@href", default=None)(self)
                if url:
                    return base_url + url

    @method
    class iter_main_page(ListElement):
        item_xpath="//div[@id='content']/section[@id='search_page']/div[@id='search_section']"

        class get_task(ItemElement):
            klass=Task

            # obj_total_results = CleanText("./div[@class='search_result_summary']")
            obj_total_pages = CleanText("./div[@id='search_result_container']/div[@class='sh_result_content mode_disp_row']" +
                "/div[@id='search_pagination']/div[@class='hr_separator']/p[@class='pagination']/a[@data-gl='pagination last page']/@data-pnum")
                

class ItemPage(HTMLPage):

    ENCODING = 'UTF8'

    def build_doc(self, text):
        return HTMLPage.build_doc(self, text)


   

    @method
    class get_item(ItemElement):
        klass = Restaurant

        obj_type = CleanText("//div[@class='establishment_row section_top section_top_playlist']" +
            "/div[@class='establishment_card']/div[@class='es_card_contain es_card_top']/div[@class='es_card_top_main']/div[@class='universe_name']")
        def obj_adresse(self):
            el = CleanText("//p[@id='eh_address']/span[@class='notranslate']", default=None)(self)
            if el:
                el = el.split('-')[0]
                return el

        obj_cp = CleanText("//p[@id='eh_address']/span[@class='notranslate']/a[@data-gl='see zip xxx']")
        obj_ville = CleanText("//p[@id='eh_address']/span[@class='notranslate']/a[@data-gl='see city xxx']")
        obj_departement = CleanText("//p[@id='eh_address']/span[@class='notranslate']/a[@data-gl='see department xxx']")
        obj_region = CleanText("//p[@id='eh_address']/span[@class='notranslate']/a[@data-gl='see region xxx']")
        def obj_pays(self):
            el = CleanText("//p[@id='eh_address']/span[@class='notranslate']", default=None)(self)

            if el:
                return el.split('- ')[-1]
        obj_telephone = CleanText("//div[@class='es_card_top_booking_phone']/p[@class='establishment_phone hide_only_print']/span[@class='button_pink btn_phone']/span")
        def obj_cadre_and_ambiance(self):
            el = CleanText("//div[@class='es_card_contain es_card_tags']/ul[@class='es_card_attr']/li[1]", default=None)(self)
            if el:
                el = el.split(':')[1]
                return re.sub(' ', '', el, 1)


        obj_cuisine = CleanText("//div[@class='es_card_contain es_card_tags']/ul[@class='es_card_attr']/li[2]/a")
        obj_budget = CleanText("//div[@class='es_card_contain es_card_tags']/ul[@class='es_card_attr']/li[3]/a")
        obj_vues = CleanText("//div[@class='es_views ']/span[@class='es_count_views']")
        obj_site_web = CleanText("//div[@class='es_card_contain es_card_external']/ul[@class='related_links']/li[@class='rl_website']/a/@href")
        obj_facebook = CleanText("//div[@class='es_card_contain es_card_external']/ul[@class='related_links']/li[@class='rl_facebook']/a/@href")
        
        def obj_image(self):
            _list = ", ".join(self.xpath("//div[@class='imgvid image_wrap ']/img/@src"))
            _list2 = ", ".join(self.xpath("//div[@class='imgvid image_wrap']/img/@data-src"))
            _list3 = ", ".join(self.xpath("//div[@class='imgvid image_wrap active']/img/@src"))
            if _list and _list2:        
                return _list +", " + _list2
            elif _list:
                return _list
            elif _list2:
                return _list2
            else:
                return _list3


            
        def obj_video(self): 
            _list = "".join(self.xpath("//script[@type='application/ld+json']/text()"))

            if 'contentUrl' in _list:
                _list = _list.split('"contentUrl":"')
                _list = _list[1].split('","embedUrl"')
                _list = base_url + _list[0].replace('\/', '/')
                return _list
            else:
                return None
        
        def obj_mots_cles(self):
            _list = ",".join(self.xpath("//div[@class='block_50 left']/div[@id='etb_base']/div[@class='etb_base_content etb_keyword_content']/a/text()"))
            _list = _list.replace(' ,', ',')
            return re.sub(' ', '', _list, 1)

        obj_description = CleanText("//div[@id='etb_desc']/div[@class='tg_content']")
        obj_parking = CleanText("//div[@class='block_50 right']/div[@class='access_infos']/div[text()='Parking']/following-sibling::span")            

        obj_lat = CleanText("//div[@class='block_50 right']/div[@class='access_infos']/span[@id='geo_eh']/span[@itemprop='latitude']")
            
        obj_lng = CleanText("//div[@class='block_50 right']/div[@class='access_infos']/span[@id='geo_eh']/span[@itemprop='longitude']")

        def obj_horaires_lundi(self):
            _list = ", ".join(self.xpath("//div[@class='horaire']/div[@class='practical_item']/table[@id='table_horaires']/tr/td[text()='Lun']/following-sibling::td/text()"))
            _list = _list.replace("-, ", "")
            if _list=='-':
                return None
            else:
                return _list
           
       
        def obj_horaires_mardi(self):
            _list = ", ".join(self.xpath("//div[@class='horaire']/div[@class='practical_item']/table[@id='table_horaires']/tr/td[text()='Mar']/following-sibling::td/text()"))
            _list = _list.replace("-, ", "")
            if _list=='-':
                return None
            else:
                return _list

        def obj_horaires_mercredi(self):
            _list = ", ".join(self.xpath("//div[@class='horaire']/div[@class='practical_item']/table[@id='table_horaires']/tr/td[text()='Mer']/following-sibling::td/text()"))
            _list = _list.replace("-, ", "")
            if _list=='-':
                return None
            else:
                return _list
        def obj_horaires_jeudi(self):
            _list = ", ".join(self.xpath("//div[@class='horaire']/div[@class='practical_item']/table[@id='table_horaires']/tr/td[text()='Jeu']/following-sibling::td/text()"))
            _list = _list.replace("-, ", "")
            if _list=='-':
                return None
            else:
                return _list
        
        def obj_horaires_vendredi(self):
            _list = ", ".join(self.xpath("//div[@class='horaire']/div[@class='practical_item']/table[@id='table_horaires']/tr/td[text()='Ven']/following-sibling::td/text()"))
            _list = _list.replace("-, ", "")
            if _list=='-':
                return None
            else:
                return _list

        def obj_horaires_samedi(self):
            _list = ", ".join(self.xpath("//div[@class='horaire']/div[@class='practical_item']/table[@id='table_horaires']/tr/td[text()='Sam']/following-sibling::td/text()"))
            _list = _list.replace("-, ", "")
            if _list=='-':
                return None
            else:
                return _list

        def obj_horaires_dimanche(self):
            _list = ", ".join(self.xpath("//div[@class='horaire']/div[@class='practical_item']/table[@id='table_horaires']/tr/td[text()='Dim']/following-sibling::td/text()"))
            _list = _list.replace("-, ", "")
            if _list=='-':
                return None
            else:
                return _list

        def obj_paiements(self):
            _list = self.xpath("//div[@class='payment']/ul[@class='block_50']/li/span[@itemprop='paymentAccepted']/text()")
            return (", ".join(_list))

        def obj_services(self):
            _list = ",".join(self.xpath("//div[@id='etb_services']/div[@class='services_content']/ul[@class='block_50']/li/a/text()"))
            _list = _list.replace(' ,', ',')
            return re.sub(' ', '', _list, 1)


class MenuPage(HTMLPage):

    def iter_menu_page(self):    
        for name in self.doc.xpath("//div[@class='menu_block']/div[@class='menu_block_content ']"):
            
            menu_obj = Menu()
            menu_obj.name = CleanText("./div[@class='menu_title_price']/div[@class='mleft menu_title']/span")(name)
            name_val = menu_obj.name.split("'")
            # print(name_val[0])
            menu_obj.price = CleanText("./div[@class='menu_title_price']/div[@class='mright menu_price']/span")(name)
            menu_obj.description = CleanText("./div[@class='menu_desc']")(name)
            menu_obj.category = CleanText("./div[@class='menu_title_price']/div[@class='mleft menu_title']" +
                "/span[contains(text(), '" + name_val[0] + 
                "')]/preceding::b[@class='h3_st'][1]")(name)
            menu_obj.sub_category = CleanText("./div[@class='menu_title_price']/div[@class='mleft menu_title']" +
                "/span[contains(text(), '" + name_val[0] + 
                "')]/preceding::div[@class='menu_block_content menu_section'][1]/div[@class='menu_title_price']/" + 
                "div[@class='mleft menu_title']/span")(name)


            date =  self.doc.xpath("//div[@id='menu']/div[@class='menu_update_date']/text()")
            date = " ".join(date)
            date = date.split()
            print(date)
            if date:
                menu_obj.last_update = date[5]

            
            yield menu_obj



